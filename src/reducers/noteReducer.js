const initState =
    {
        notes: []
    };
export default (state = initState, action) => {
    switch (action.type) {
        case 'GET_ALL_NOTES':
            return{
                ...state,
                notes: action.notes
            };

        default:
            return state;
    }
};