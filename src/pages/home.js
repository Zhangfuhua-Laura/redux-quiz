import React, {Component} from 'react';
import {getAllNotes} from "../actions/notesActions";
import {bindActionCreators} from "redux";
import {BrowserRouter as Router, Link, Route} from "react-router-dom";
import {connect} from "react-redux";
import NoteDetails from "../components/noteDetails";

class Home extends Component {

    constructor(props, context) {
        super(props, context);
    }

    componentDidMount() {
        this.props.getAllNotes();
    }

    render() {
        return (
            <div>
                {this.props.notes.map(item => {
                    return (
                        <div className="noteTitle">
                            <Link to={{
                                pathname: `/notes/${item.id}`,
                                title: `${item.title}`,
                                description: `${item.description}`
                            }}>{item.title}</Link>
                        </div>);
                })}
                <div className="noteTitle">
                    <Link to="/note/create">create</Link>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    notes: state.noteReducer.notes
});

const mapDispatchToProps = dispatch => bindActionCreators({
    getAllNotes
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Home);