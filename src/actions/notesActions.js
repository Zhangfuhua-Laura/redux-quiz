export const getAllNotes = () => (dispatch) => {
    fetch('http://localhost:8080/api/posts')
        .then(response => response.json())
        .then(result => {
            dispatch({
                type: 'GET_ALL_NOTES',
                notes: result
            });
        });
};