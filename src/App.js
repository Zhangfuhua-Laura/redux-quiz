import React, {Component} from 'react';
import {BrowserRouter as Router, Route} from "react-router-dom";
import './App.less';
import Home from "./pages/home";
import NoteDetails from "./components/noteDetails";
import CreateNote from "./components/createNote";

class App extends Component{
  render() {
    return (
      <div className='App'>
        <Router>
          <Route exact path="/" component={Home}/>
          <Route path="/notes/:id" component={NoteDetails}/>
          <Route exact path="/note/create" component={CreateNote}/>
        </Router>
      </div>
    );
  }
}

export default App;