import React, {Component} from 'react';

class CreateNote extends Component {

    constructor(props, context) {
        super(props, context);
        this.submitNewNote = this.submitNewNote.bind(this);
    }

    render() {
        return (
            <div>
                <label>标题<br/><input/><br/></label>
                <label>正文<br/><textarea/><br/></label>
                <button onClick={this.submitNewNote}>提交</button>
                <button>取消</button>
            </div>
        );
    }

    submitNewNote() {
        fetch('http://localhost:8080/api/posts', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({"title":"aaa","description":"bbb"})
        }).then(
            this.props.history.push("/")
        );
    }
}

export default CreateNote;