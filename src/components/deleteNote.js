import React, {Component} from 'react';
import {Redirect} from "react-router-dom";

class DeleteNote extends Component {

    constructor(props, context) {
        super(props, context);
        this.deleteNote = this.deleteNote.bind(this);
    }

    render() {
        return (
            <div>
                <button onClick={this.deleteNote}>删除</button>
            </div>
        );
    }


    deleteNote() {
        fetch('http://localhost:8080/api/posts/'+this.props.id, {
            method: 'DELETE'
        }).then(
            // this.props.history.push("/")
            {/*<Redirect to="/"/>*/}
        );
    }
}

export default DeleteNote;