import React, {Component} from 'react';
import DeleteNote from "./deleteNote";

class NoteDetails extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div>
                id: {this.props.match.params.id}<br/>
                title: {this.props.location.title}<br/>
                description: {this.props.location.description}
                <DeleteNote id={this.props.match.params.id}/>
            </div>
        );
    }
}

export default NoteDetails;