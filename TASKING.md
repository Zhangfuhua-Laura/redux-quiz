## get all notes from api and store in status
* create home page with all notes content
* trigger get notes event 
* create action and fetch data
* create reducer to handle the action
* mapDispatchToProps
* mapStateToProps

## create home page with Link to notes Page and createNote Page
* the notes data should be from states
* link to note details and link to createNote page

## notes details page 
* left aside shows the list of notes
* right aside shows the title and body
* background-color and scroll style

## create note
* create note input
* if title or body is null, then disable create button
* after create
